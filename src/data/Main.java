package data;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TexturePaint;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;
import javax.swing.JCheckBox;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.Toolkit;

public class Main extends JFrame {

	private JPanel contentPane;
	private JTextField textPath;
	
	private JFileChooser dialog = new JFileChooser();
	private File folder;
	private JTextField textType;
	
	private String rememberPath = "";
	
	private ArrayList<File> allFiles = new ArrayList<File>();
	private ArrayList<File> filesForCheck = new ArrayList<File>();
	private ArrayList<File> pendingFiles = new ArrayList<File>();
	private ArrayList<File> doneDirectories = new ArrayList<File>();
	
	private DefaultListModel<String> fileModel = new DefaultListModel<String>();
	private JLabel lblResult;
	private JTextField textFile;
	private JList listResults;
	private JLabel lblWorking;
	
	private final String hiddenSuffix = " - hidden";
	private JCheckBox chckbxLiveSearch;
	private JCheckBox chckbxOpenFileOn;
	private JButton btnOpenDestination;
	private JButton btnSaveResults;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("resources" + File.separator + "logo.png"));
		setTitle("Magnifyle");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 986, 364);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][grow][][grow][][]", "[][][][][grow][][]"));
		
		JLabel lblPath = new JLabel("Path");
		contentPane.add(lblPath, "cell 0 0,alignx trailing");
		
		textPath = new JTextField();
		contentPane.add(textPath, "flowx,cell 1 0 3 1,growx");
		textPath.setColumns(10);
		
		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				search();
			}
		});
		contentPane.add(btnSelect, "cell 4 0");
		
		chckbxLiveSearch = new JCheckBox("Live Search");
		chckbxLiveSearch.setSelected(true);
		contentPane.add(chckbxLiveSearch, "cell 5 0");
		
		JLabel lblFile = new JLabel("File");
		contentPane.add(lblFile, "cell 0 1,alignx trailing");
		
		textFile = new JTextField();
		textFile.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				liveSearch();
			}
		});
		textFile.setText("*");
		contentPane.add(textFile, "cell 1 1,growx");
		textFile.setColumns(10);
		
		JLabel lblType = new JLabel("Type");
		contentPane.add(lblType, "cell 2 1,alignx trailing");
		
		textType = new JTextField();
		textType.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				liveSearch();
			}
		});
		textType.setText("*");
		contentPane.add(textType, "cell 3 1,growx");
		textType.setColumns(10);
		
		JButton btnFind = new JButton("Find");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				findFiles(false);
			}
		});
		contentPane.add(btnFind, "cell 4 1,growx");
		
		lblWorking = new JLabel("Working");
		lblWorking.setVisible(false);
		
		chckbxOpenFileOn = new JCheckBox("Open File on click ");
		chckbxOpenFileOn.setSelected(true);
		contentPane.add(chckbxOpenFileOn, "cell 5 1");
		contentPane.add(lblWorking, "cell 0 2");
		
		lblResult = new JLabel("Result");
		contentPane.add(lblResult, "cell 0 3 5 1");
		
		JScrollPane scrollPaneResults = new JScrollPane();
		contentPane.add(scrollPaneResults, "cell 0 4 5 1,grow");
		
		listResults = new JList();
		listResults.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				openExplorer();
			}
		});
		listResults.setModel(fileModel);
		listResults.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneResults.setViewportView(listResults);
		
		btnOpenDestination = new JButton("Open Destination");
		btnOpenDestination.setEnabled(false);
		btnOpenDestination.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				openExplorer();
			}
		});
		contentPane.add(btnOpenDestination, "cell 0 5 5 1,growx");
		
		btnSaveResults = new JButton("Save Results");
		btnSaveResults.setEnabled(false);
		btnSaveResults.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveResults();
			}
		});
		contentPane.add(btnSaveResults, "cell 0 6 5 1,growx");
	}

	
	public void search()
	{		
		
		dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int status = dialog.showOpenDialog(null);
		if (status == 0)
		{
			folder = dialog.getSelectedFile();
			textPath.setText(folder.getAbsolutePath());
		}
	}
	
	public void findFiles(boolean live)
	{
		String name = textFile.getText();
		String type = textType.getText();
		
		boolean newFile = true;
		
		String elementName = "";
		
		fileModel.clear();
		doneDirectories.clear();
		
		if (!live)
		{
			if (loadFromSave())
			{
				rememberPath = textPath.getText();
			}
		}			
		if (!rememberPath.equals(textPath.getText()))
		{
			allFiles.clear();
			rememberPath = textPath.getText();
			for(String each:folder.list())
			{
				filesForCheck.add(new File(folder + File.separator + each));
			}
			
			while(newFile)
			{
				newFile = false;
				for(File each: filesForCheck)
				{
					if (each.isDirectory())
					{
						if (!doneDirectories.contains(each))
						{
							String[] subDirectory = each.list();
							if (subDirectory != null)
							{
								for(String file:each.list())
								{
									pendingFiles.add(new File(each + File.separator + file));
								}
							}
							doneDirectories.add(each);
						}
					}
					allFiles.add(each);
				}
				filesForCheck.clear();
				for(File each: pendingFiles)
				{
					if (!allFiles.contains(each))
					{
						filesForCheck.add(each);
						newFile = true;
					}
				}
				pendingFiles.clear();
			}
			
			for (File each: allFiles)
			{
				if (fileModel.size() < 100000)
				{
					if (type.equals(each.getAbsolutePath().split("\\.")[each.getAbsolutePath().split("\\.").length-1]) || type.equals("*"))
					{
						if(each.getAbsolutePath().toLowerCase().split("\\\\")[each.getAbsolutePath().split("\\\\").length-1].contains(name.toLowerCase()) || name.equals("*"))
						{
							elementName = each.getAbsolutePath();
							if (each.isHidden())
								elementName += hiddenSuffix;
							fileModel.addElement(elementName);
						}
							
					}
				}
				else
					break;
			}
		}

		else
		{
			fileModel.clear();
			for (File each: allFiles)
			{
				
				if (fileModel.size() < 100000)
				{
					String[] splittedTypes = type.split(",");
					for (String typeFilter: splittedTypes)
					{
						if (typeFilter.toLowerCase().trim().equals(each.getAbsolutePath().split("\\.")[each.getAbsolutePath().split("\\.").length-1].toLowerCase()) || type.equals("*") || type.equals(""))
						{
							String[] splittedValues = name.split(",");
							for (String nameFilter: splittedValues)
							{
								if(each.getAbsolutePath().toLowerCase().split("\\\\")[each.getAbsolutePath().split("\\\\").length-1].split("\\.")[0].contains(nameFilter.toLowerCase().trim()) || name.equals("*") || name.equals(""))
								{
									elementName = each.getAbsolutePath();
									if (each.isHidden())
										elementName += hiddenSuffix;
									fileModel.addElement(elementName);
								}
							}
						}
					}
				}
				else
					break;
			}
		}
		if (fileModel.getSize() > 0)
		{
			btnOpenDestination.setEnabled(true);
			btnSaveResults.setEnabled(true);
		}
		else
		{
			btnOpenDestination.setEnabled(false);
			btnSaveResults.setEnabled(false);
		}
			
		lblResult.setText("Results - " + fileModel.getSize() + " Files/Directories");
		lblWorking.setVisible(false);
	}
	
	public void openExplorer()
	{
		try
		{
			if (!listResults.isSelectionEmpty() && listResults.getSelectedIndex() != -1 && chckbxOpenFileOn.isSelected())
			{
				Runtime.getRuntime().exec("explorer.exe /select," + listResults.getSelectedValue().toString().replaceAll(hiddenSuffix, ""));
			}
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}
	
	public void liveSearch()
	{
		if (!allFiles.isEmpty() && chckbxLiveSearch.isSelected())
		{
			findFiles(true);
		}
	}
	
	public void saveResults()
	{
		BufferedWriter write = null;
		File saves = new File("savedSearches.txt");
		
		try
		{
			
			if (!readSaves())
			{
				write = new BufferedWriter(new FileWriter(saves, true));
				write.write(rememberPath);
				write.newLine();
				for (File each: allFiles)
				{
					write.write(each.getAbsolutePath().replaceAll(rememberPath.replace("\\", "\\\\"), ""));
					write.newLine();
				}
			}
		}
		catch(IOException ex)
		{
			
		}
		finally
		{
			try
			{
				if (write != null)
					write.close();
			}
			catch (IOException ex)
			{
				
			}
		}
	}
	
	public boolean readSaves()
	{
		BufferedReader read = null;
		
		File saves = new File("savedSearches.txt");
		boolean saveFound = false;
		String line;
		
		try
		{
			read = new BufferedReader(new FileReader(saves));
			while ((line = read.readLine()) != null && !saveFound)
			{
				if(line.equals(rememberPath))
				{
					saveFound = true;
				}
			}
		}
		catch(IOException ex)
		{
			
		}
		finally
		{
			try
			{
				if (read != null)
					read.close();
			}
			catch (IOException ex)
			{
				
			}
			
		}
		return saveFound;
	}
	
	public boolean loadFromSave()
	{
		BufferedReader read = null;
		
		ArrayList<File> rememberFiles = new ArrayList<File>();
		File saves = new File("savedSearches.txt");
		boolean readingDone = false;
		String line;
		
		boolean foundSave = false;
		
		for (File each:allFiles)
		{
			rememberFiles.add(each);
		}
		
		allFiles.clear();
		fileModel.clear();
	
		try
		{
			read = new BufferedReader(new FileReader(saves));
			while ((line = read.readLine()) != null && !readingDone)
			{
				if(line.equals(textPath.getText()))
				{
					line = read.readLine();
					while (!line.contains(":\\"))
					{
						fileModel.addElement(textPath.getText() + line);
						allFiles.add(new File(textPath.getText() + line));
						line = read.readLine();
						if (line == null)
							break;
					}
					readingDone = true;
					foundSave = true;
				}
			}
		}
		catch(IOException ex)
		{
			
		}
		finally
		{
			try
			{
				if (read != null)
					read.close();
			}
			catch (IOException ex)
			{
				
			}
		}
		if (!foundSave)
		{
			for (File each:rememberFiles)
			{
				allFiles.add(each);
			}
		}
		return foundSave;
	}	
}
